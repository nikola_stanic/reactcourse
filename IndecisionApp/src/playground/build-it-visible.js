console.log("Build it visible in work!");

// const appRoot = document.getElementById("app");
// let visible = false;

// const render = () => {
//     const visibilityTemplate = (
//         <div>
//             <h1>Visibility toggle</h1>
//             <button onClick={toggleMyText}>{visible?"Hide details":"Show details"}</button>
//             {visible && (
//                 <p>Hey here's some text!</p>
//             )}
//         </div>
//     );
//     ReactDOM.render(visibilityTemplate,appRoot);
// }


// const toggleMyText = ()=>{
//     if(visible){
//         visible = false;
//         render();
//     }
//     else {
//         visible = true;
//         render();
//     }
// }

// render();

class VisibilityToggle extends React.Component{

    constructor(props){
        super(props);
        this.state = {
            visibility:false
        }
        this.handleToggleVisibility = this.handleToggleVisibility.bind(this);
    }

    handleToggleVisibility(){
        this.setState((prevState)=>{
            return{
                visibility:!prevState.visibility
            }
        });
    }

    render(){
        return (
            <div>
                <h1>Visibility toggle</h1>
                <button onClick={this.handleToggleVisibility}>
                    {this.state.visibility?"Hide details":"Show details"}
                </button>
                {this.state.visibility && (
                    <div>
                        <p>Hey these are some details for you to see!</p>
                    </div>
                )}
            </div>
        );
    }
}

ReactDOM.render(<VisibilityToggle/>,document.getElementById("app"));