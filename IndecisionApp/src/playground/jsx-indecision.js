const app = {
    title: "Indecision App",
    subtitle:"Put your life in the hands of a computer",
    options: []
};

const onFormSubmit = (event) =>{
    event.preventDefault();
    const option = event.target.elements.option.value;
    if(option){
        app.options.push(option);
        event.target.elements.option.value = "";
        renderOptionsApp();
    }
}

const removeAllOptions = (event) => {
    event.preventDefault();
    app.options = [];
    renderOptionsApp();
}

const onMakeDecision = () =>{
    const randomNum = Math.floor(Math.random() * app.options.length);
    const option = app.options[randomNum];
    alert(option);
}

const appRoot = document.getElementById("app");


const renderOptionsApp = () =>{
    const template = (
        <div>
            <h1>{app.title}</h1>
            {app.subtitle? <p>{app.subtitle}</p>:"n/a"}
            <p>{app.options.length>0 ? "Here are your options":"No options"}</p>
            <button disabled={app.options.length === 0} onClick={onMakeDecision}>What should I do?</button>
            <button onClick={removeAllOptions}>Remove all</button>
            <ol>
                {
                    app.options.map((option)=><li key={option}>{option}</li>)
                }
            </ol>
            <form onSubmit={onFormSubmit}>
                <input type="text" name="option"/>
                <button>Add Option</button>
            </form>
        </div>
    );

    ReactDOM.render(template,appRoot);
}

renderOptionsApp();

/*
 React.createElement creates templates and returns variables of type Object
 which has set of properties on of which is children which are DOM objects 
 to be rendered inside this object.
*/