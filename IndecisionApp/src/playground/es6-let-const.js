var nameVar = "nikola";
var nameVar = "andrew";
console.log("nameVar",nameVar);

let nameLet = "jen";
nameLet = "Julie";
console.log("nameLet",nameLet);

const nameConst = "Frank";
console.log("nameConst",nameConst);

// Block scoping
/* 
    var - function scoped
    let, const - block scoped
*/
var fullName = "Nikola Stanic";

if (fullName){
    const firstName = fullName.split(" ")[0];
    console.log(firstName);
}

console.log(firstName); // This will throw an error because const (or let) type is block scoped