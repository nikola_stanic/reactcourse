console.log("Counter example is running!")

class Counter extends React.Component{

    constructor(props){
        super(props);
        this.handleAddOne = this.handleAddOne.bind(this);
        this.handleMinusOne = this.handleMinusOne.bind(this);
        this.handleReset = this.handleReset.bind(this);
        this.state = {
            count:props.count
        };
    }

    handleAddOne(){
        this.setState((prevState)=>{
            return{
                count : prevState.count + 1
            };
        });
    }

    handleMinusOne(){
        this.setState((prevState)=>{
            return{
                count: prevState.count - 1
            };
        });
    }

    handleReset(){
        console.log("handleReset");
    }

    render() {
        return (
          <div>
            <h1>Count: {this.state.count}</h1>
            <button onClick={this.handleAddOne}>+1</button>
            <button onClick={this.handleMinusOne}>-1</button>
            <button onClick={this.handleReset}>reset</button>
          </div>
        );
      }
}

Counter.defaultProps = {
    count : 0
}

ReactDOM.render(<Counter count={-1}/>,document.getElementById("app"));

// let count = 0;
// const someId = "my-id";
// const buttonMinusId = "my-id-minus"
// const buttonResetId = "my-id-reset";
// const addOne = () => {
//     count++;
//     renderCounterApp();
// }

// const minusOne = () => {
//     count--;
//     renderCounterApp();
// }

// const reset = () => {
//     count=0;
//     renderCounterApp();
// }



// const renderCounterApp = () => {
//     const templateTwo = (
//         <div>
//             <h1>Count: {count}</h1>
//             <button id={someId} className="button" onClick={addOne}>+1</button>
//             <button id={buttonMinusId} className="button" onClick={minusOne}>-1</button>
//             <button id={buttonResetId} className="button" onClick={reset}>Reset</button>
//         </div>
//     );

//     ReactDOM.render(templateTwo, appRoot);
// }

// renderCounterApp();