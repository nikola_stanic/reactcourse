// arguments object - no longer bound with arrow functions
const add = (a,b) =>{
    //console.log(arguments);
    return a + b;
};
console.log(add(55,1,1001));
// this keyword - no longer bound

const user = {
    name : "nikola",
    cities: ["Banja Luka", "Beograd", "Ann Arbor"],
    printPlacesLived(){
        return this.cities.map((city)=> this.name+ " has lived in " + city);
        /*
            Ako koristimo klasicne JS funkcije console.log iz forEach petlje za gradove
            nece raditi jer this nije bound-ovan za okolni blok nego samo za forEach blok.

            Dakle klasicne JS funkcije vezuju =vlastiti this=.

            Arrow funkcije ne bounduju vlastiti this nego koriste =this roditeljskog konteksta=.
            To znaci da ce u ovom slucaju za arrow funciju raditi this.name jer ce this biti vezan
            za kontekst user-a.s

        */

    }
};

const multiplier = {
    numbers : [1,2,3],
    multiplyBy : 2,
    multiply(){
        return this.numbers.map((currNum)=>currNum * this.multiplyBy);
    }
}

console.log(user.printPlacesLived());
console.log(multiplier.multiply());
