import { createStore } from 'redux';

// Action generators - functions that return action objects
/*
    ({ incrementBy = 1 } = {})

    How does incrementBy default to 1 in this situation? 
        1) If an object is provided and there is no property 'incrementBy' 
        2) If there is no object provided: default is going to be empty object ( {} ) and 
            when destructuring the empty object it's again not going to have 'incrementBy' 
            so the default value is going to be set to 1 again.
*/

/*
    Function incrementCount doesn't have a return statement beacuse it's body is wrapped with (<incrementCountBody>) which
    will implicitly return an object as return value - implicit return.
 */

const incrementCount = ({ incrementBy = 1 } = {}) => ({
    type: 'INCREMENT',
    incrementBy
});

const decrementCount = ({ decrementBy = 1 } = {}) => ({
    type: 'DECREMENT',
    decrementBy
});

const setCount = ({count}) => ({
    type: 'SET',
    count
});

const resetCount = () => ({
    type: 'RESET'
});

const store = createStore((state = { count: 0 }, action) => {
    switch (action.type) {
        case 'INCREMENT':
            return {
                count: state.count + action.incrementBy
            };
        case 'DECREMENT':
            return {
                count: state.count - action.decrementBy
            };
        case 'RESET':
            return {
                count: 0
            }
        case "SET":
            return{
                count: action.count
            }
        default:
            return state;
    }
    return state;
});

const unsubscribe = store.subscribe(() => {
    {
        console.log(store.getState());
    }
})

store.dispatch(incrementCount({ incrementBy:5 }));
store.dispatch(incrementCount());

store.dispatch(resetCount());

store.dispatch(decrementCount());

store.dispatch(decrementCount({decrementBy:10}));

store.dispatch(setCount({count:-100}));
