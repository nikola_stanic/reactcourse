// --- Object destructuring ---

// const person = {
//     name:"Nikola",
//     age:24,
//     location: {
//         city: "Ann Arbor, MI",
//         temp: 16
//     }
// }

// // default name set to 'Anonymous' (if person object has no property name -> use 'Anonymous')
// const {name:firstName = 'Anonymous',age} = person;
// console.log(`${firstName} is ${age}.`);

// // renaming temp property to 'temperature' - after this renaming temp will no longer exist
// const { city, temp:temperature } = person.location;
// if(city && temperature){
//     console.log(`It's ${temperature} [deg. Celsius] in ${city}.`)
// }

// const book = {
//     title: "Ego is the Enemy",
//     author: "Ryan Holiday",
//     publisher: {
//         name: "Penguin"
//     }
// }

// const {name:publisherName="Self-Published"} = book.publisher;

// console.log(publisherName); // Penguin, Self-Published


// --- Array destructuring ---

const address = ['1299 S Juniper Street', 'Philadelphia', 'Pennsylvania', '19147'];
const [, city, state='New York'] = address;
console.log(`You are in ${city} ${state}`);

const item = ["Coffee (hot)", "$2.00", "$2.50", "$2.75"];
const [itemName, smallPrice, mediumPrice, largePrice] = item;
console.log(`A medium ${itemName} costs ${mediumPrice}.`);