import React from 'react';
import { connect } from 'react-redux';
import { setTextFilter } from '../actions/filters';
import { DateRangePicker } from 'react-dates';
import { sortByDate, sortByAmount, setStartDate, setEndDate } from '../actions/filters';

class ExpenseListFilters extends React.Component{

    state = {
        calendarFocused:null
    };

    onDatesChange = ({ startDate, endDate }) => {
        this.props.dispatch(setStartDate(startDate));
        this.props.dispatch(setEndDate(endDate));
    };

    onFocusChange = (calendarFocused) => {
        this.setState(()=> ({ calendarFocused }))
    };

    render(){
        return (
        <div>
        <input type="text" value={this.props.filters.text} onChange={(e)=>{
            this.props.dispatch(setTextFilter(e.target.value));
        }} />
        <select value={this.props.filters.sortBy} onChange={(e)=>{
            if(e.target.value==='date'){
                this.props.dispatch(sortByDate());
            }
            else if(e.target.value=='amount'){
                this.props.dispatch(sortByAmount());
            }
        }}>
            <option value="date">Date</option>
            <option value="amount">Amount</option>
        </select>
        
        <DateRangePicker
            startDate={this.props.filters.startDate}
            endDate={this.props.filters.endDate}
            onDatesChange={this.onDatesChange}
            focusedInput={this.state.calendarFocused}
            onFocusChange={this.onFocusChange}
            showClearDates={true}
            numberOfMonths={1}
            isOutsideRange={()=>false}
        />

    </div>
        )}
}

const mapStateToProps = (state) => {
    return {
        filters: state.filters
    };
};

/*
    What is the purpose of mapStateToProps? 

    -   The purpose is to have a function that will receive whole state from the store
    and map (grab from the store) to props only the properties of the store that this component needs.
*/

/*
    What do we need in order to displatch actions to our store? 
    
    -   We need conenct method from react-redux library that will connect this component
    to our redux store - in that way this component will have access to the method dispatch.
*/
export default connect(mapStateToProps)(ExpenseListFilters);

/*
    Why are we exporting connected version of ExpenseListFilters? 
    
    -   It allows us to dispatch actions while the basic ExpenseListFilters wouldn't allow us to dispatch actions.
*/