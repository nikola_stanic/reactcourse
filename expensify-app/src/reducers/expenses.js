// Expenses Reducer
const expensesReducerDefaultState = [];

export default (state = expensesReducerDefaultState,action) => {
    switch(action.type){
        case 'ADD_EXPENSE':
            return [
                ...state, // ES6 'spread' opreator - doesn't change existing state, just creates a new array with existing state
                action.expense
            ];
        case 'REMOVE_EXPENSE':
            return state.filter(({id})=> id!==action.expenseId);
        case 'EDIT_EXPENSE':
            return state.map((expense)=>{
                if(expense.id===action.id){
                    return {
                        ...expense,
                        ...action.updates // Object spreading
                    };
                }
                else{
                    return expense;
                }
            })
        default: return state;
    }
};
